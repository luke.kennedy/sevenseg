export const ComponentName = 'seven-seg';

export enum DisplayType {
	SevenSeg = "seven-seg",
	NineSeg = "nine-seg",
	FourteenSeg = "fourteen-seg",
	SixteenSeg = "sixteen-seg"
}

export class SevenSegProperties {
	/** The string or number to display on the 7seg characters */
	displayValue?: string | number = undefined;
	/** The maximum number of characters which can be used to display the value */
	maxCharacters?: number = undefined;
	/** The minimum number of characters which must exist */
	minCharacters?: number = undefined;
	/** Enable 9/14/16 segment mode for high precision */
	displayType?: DisplayType = undefined;
}

export class SevenSeg extends HTMLSpanElement {
	/// Setup
	private props: SevenSegProperties = {
		displayType: DisplayType.SevenSeg
	};
	constructor() {
		super();
		this.attachShadow({
			mode: "open"
		})
	}

	/// Getters and setters
	get displayValue(): string | number | undefined {
		return this.props.displayValue;
	}
	set displayValue(v: string | number | undefined) {
		this.props.displayValue = v;
		this.setAttribute("displayValue", `${v ?? ""}`);
	}
	get maxCharacters(): number | undefined {
		return this.props.maxCharacters;
	}
	set maxCharacters(v: number | undefined) {
		this.props.maxCharacters = v;
		this.setAttribute("maxCharacters", `${v ?? ""}`);
	}
	get minCharacters(): number | undefined {
		return this.props.minCharacters;
	}
	set minCharacters(v: number | undefined) {
		this.props.minCharacters = v;
		this.setAttribute("minCharacters", `${v ?? ""}`);
	}
	get displayType(): DisplayType | undefined {
		return this.props.displayType;
	}
	set displayType(v: DisplayType | undefined) {
		this.props.displayType = v;
		this.setAttribute("displayType", v ?? "");
	}
	attributeChangedCallback(name: string, oldValue: string, newValue: string) {
		console.log('Custom element attributes changed.');
		const validProps = Object.keys(this.props);
		if (!validProps.includes(name)) {
			// Invalid prop
			throw new Error(`Invalid property on seven-seg: ${name}`);
		}
		switch (name) {
			case "displayValue":
				// TODO: handle number vs. string
				this.props.displayValue = newValue;
				break;
			case "maxCharacters":
				break;
			case "minCharacters":
				break;
			case "displayType":
				break;
			default:
				// Unhandled prop
				throw new Error(`Unimplemented property on seven-seg: ${name}`);
		}
	}

	/// Lifecycle methods
	connectedCallback() {
		console.log('Custom element added to page.');
	}
	disconnectedCallback() {
		console.log('Custom element removed from page.');
	}
	adoptedCallback() {
		console.log('Custom element moved to new page.');
	}
}
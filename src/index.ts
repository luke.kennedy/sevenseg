import * as sevenseg from './SevenSeg';

customElements.define(sevenseg.ComponentName, sevenseg.SevenSeg, {
	extends: 'span'
})
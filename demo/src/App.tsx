import React, { HTMLAttributes } from 'react';
import logo from './logo.svg';
import './App.css';

import "../../src";
import { SevenSegProperties } from '../../src/SevenSeg';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      'seven-seg': SevenSegAttrs;
    }

    interface SevenSegAttrs extends HTMLAttributes<HTMLSpanElement>, SevenSegProperties {
    }
  }
}

function App() {
  return (
    <div className="App">
      <seven-seg displayValue={100}></seven-seg>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
